package com.aj.imageupload;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.json.JSONObject;

import java.io.File;

public class MainActivity extends AppCompatActivity {


    int pic = 0;
    private Uri mOutputUri;
    private File mFile1 = null;
    ImageView img;
    Button btn_upload;
    private MultiPartRequest mMultiPartRequest;
    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img = (ImageView) findViewById(R.id.img);
        btn_upload = (Button) findViewById(R.id.btn_upload);

        progressDialog = new ProgressDialog(this);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pic = 1;
                selectImage();
            }
        });

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile(mFile1);
                progressDialog.setMessage("Uploading");
                progressDialog.show();

            }
        });
    }

    private void selectImage() {
        Log.e("MASUK", "ADD IMG");
        final CharSequence[] items = {getString(R.string.camera), getString(R.string.gallery), getString(R.string.back)};

        final CharSequence[] items2 = {getString(R.string.camera), getString(R.string.gallery), getString(R.string.remove), getString(R.string.back)};


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.selectImg);
        if ((pic == 1 && mFile1 == null)) {

            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (items[item].equals(getString(R.string.camera))) {

                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        mOutputUri = FileManager.getOutputMediaFileUri(Template.Code.CAMERA_IMAGE_CODE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputUri);
                        startActivityForResult(intent, Template.Code.CAMERA_IMAGE_CODE);

                    } else if (items[item].equals(getString(R.string.gallery))) {

                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("image/*");
                        startActivityForResult(intent, Template.Code.FILE_MANAGER_CODE);

                    } else if (items[item].equals(getString(R.string.back))) {
                        Log.e("PHOTO", "DISMISS");
                        dialog.dismiss();
                    }
                }
            });

        } else {

            builder.setItems(items2, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (items2[item].equals(getString(R.string.camera))) {
                        Log.e("PHOTO", "CAMERA " + items[item]);

                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        mOutputUri = FileManager.getOutputMediaFileUri(Template.Code.CAMERA_IMAGE_CODE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputUri);
                        startActivityForResult(intent, Template.Code.CAMERA_IMAGE_CODE);

                    } else if (items2[item].equals(getString(R.string.gallery))) {
                        Log.e("PHOTO", "GALLERY");

                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("image/*");
                        startActivityForResult(intent, Template.Code.FILE_MANAGER_CODE);

                    } else if (items2[item].equals(getString(R.string.remove))) {
                        Log.e("PHOTO", "REMOVE");
                        resetPic(pic);

                    } else if (items2[item].equals(getString(R.string.back))) {
                        Log.e("PHOTO", "DISMISS");
                        dialog.dismiss();
                    }
                }
            });


        }
        builder.show();
    }

    void resetPic(int gambar) {
        if (gambar == 1) {
            mFile1 = null;
            Glide.with(this).load(R.mipmap.ic_launcher).into(img);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Log.e("REQ", requestCode + "");
            Log.e("RES", resultCode + "");
            if (requestCode == Template.Code.FILE_MANAGER_CODE) {
                setFile(requestCode, data.getData());
                setView(requestCode, data.getData());
            } else {
                setFile(requestCode, mOutputUri);
                setView(requestCode, mOutputUri);
            }

        } else {

        }
    }

    void setFile(int type, Uri uri) {
        if (pic == 1) {
            mFile1 = new File(FileManager.getPath(getApplicationContext(), type, uri));
            mFile1 = CompressImage.saveBitmapToFile(mFile1);
        }
    }

    void setView(int type, Uri uri) {

        Log.e("setView", type + "");
        if (type == Template.Code.CAMERA_IMAGE_CODE) {

            if (pic == 1) {
                Glide.with(this).load(uri).into(img);
            }

        } else {

            File file = new File(FileManager.getPath(getApplicationContext(), type, uri));
            Log.e("IMG GALLERY", file.toString());
            int fileType = FileManager.fileType(file);
            if (fileType == Template.Code.CAMERA_IMAGE_CODE) {
                if (pic == 1) {
                    Glide.with(this).load(uri).into(img);
                }
            }

        }
    }

    void uploadFile(File strFile) {

        Log.e("strFile", strFile.toString() + "");

        Log.e("******", "**************");

        String[] param = {};
        String[] key = {};
        mMultiPartRequest = new MultiPartRequest(new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                setResponse(null);
            }
        }, new Response.Listener() {
            @Override
            public void onResponse(Object response) {

                setResponse(response);
            }
        }, strFile, "http://image.appreka.com.my/pic", key, param);
        mMultiPartRequest.setTag("MultiRequest");
        mMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(1000 * 100,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(this).addToRequestQueue(mMultiPartRequest);
    }

    void setResponse(Object response) {
        Log.e("RES", response + "");
        String res = response.toString();
        progressDialog.dismiss();

        if (response != null) {
            try {
                JSONObject jsonObj = new JSONObject(res);
                String status = jsonObj.getString("status");
//                String message = jsonObj.getString(LinkPath.TAG_SCHE_MSG);

                if (status.equals("success")) {

                    Toast.makeText(this, "Image successfully uploaded.", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(this, "Image cannot be uploaded.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception ex) {

            }
        } else {
            Toast.makeText(this, "Image cannot be uploaded", Toast.LENGTH_SHORT).show();
        }
    }
}
